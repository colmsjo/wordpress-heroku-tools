Running locally on OSX
======================

Start mysql server and apache
```
sudo apachectl start
mysql.server start
```

Deploy php scripts
```
sudo ./deploy-locally.sh wp-site
```


Importing and exporting
-----------------------

Export database (or use the export-cleardb.sh script):
```
heroku config # show the database configuration

mysqldump -uUSER -pPASSWORD -hus-cdbr-east-02.cleardb.com DATABASE > data.sql
```


It is possible to import a database on a server where you have phpMyAdmin (or some other tool).

1. Import the database dump:
```
mysql -uUSER -pPASSWORD -hHOST DATABASE < data.sql
```

2. Change the field site_url in the table wp_options

3. Login to the admin interface and change Settings->General->URL



Troubleshooting
---------------

Need to install php with mysqli

```
brew install php54 --with-mysql
```
