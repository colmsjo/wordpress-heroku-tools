Apache on OSX 
=============

Wordpress needs to be installed in the root directory of the web server.
Link for media rely on this, /wp-content/img/... is used for logotypes etc.

The easiest way to achieve this is to setup a virtual host. An example of an 
apache vhost file in is included in this folder. Modify /etc/apache2/extra/httpd-vhosts.conf
accordingly.

Also, make sure that the httpd-vhosts.conf is included from httpd.conf (typically commented out).

In addition, hosts need to be setup locally. The suggested approach is to setup serveral localhost names.

Update /etc/hosts

```
# Used by apache virtal hosts
127.0.0.1	localhost1
127.0.0.1	localhost2
127.0.0.1	localhost3
...
```


NOTE: Don't forget to restart apache after changed the httpd configuration: sudo apachectl restart
