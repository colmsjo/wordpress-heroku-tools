#!/bin/bash

if [ -n "$1" ]
then
    echo Deploying $1
else

	# Show usage and exit

    echo Usage: ./deploy-locally [wordpress site]
    exit 0
fi

export DEST="/Library/WebServer/Documents/$1"

sudo rm    -rf $DEST
sudo mkdir     $DEST

sudo cp -R $1/* $DEST
sudo cp    $1/.htaccess $DEST
sudo cp    $1/wp-config.php.traditional $DEST/wp-config.php


sudo chown -R _www:_www $DEST
sudo chmod ug+w         $DEST
sudo chmod -R ug+w      $DEST

echo "Contents of destination"
ls $DEST
