#!/bin/bash

if [ -n "$1" ]
then
    echo Exporting Cleardb Mysql database $1
else

	# Show usage and exit

    echo Usage: ./export-cleardb [folder with cleardb-credentials file]
    exit 0
fi

source $1/cleardb-credentials

mysqldump -u$DBUSER -p$DBPASSWD -h$DBHOST $DBNAME > $1/cleardb-data.sql

