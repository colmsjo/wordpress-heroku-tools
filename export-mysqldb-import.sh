#!/bin/bash

if [ -n "$1" ]
then
    echo Exporting Mysql database $1 and importing into cleardb
else

	# Show usage and exit

    echo Usage: ./export-mysql-import.sh [folder with mysqldb-credentials file]
    exit 0
fi

./website-dev-tools/export-mysqldb.sh $1
cp $1/mysqldb-data.sql $1/cleardb-data.sql
./website-dev-tools/import-cleardb.sh $1

