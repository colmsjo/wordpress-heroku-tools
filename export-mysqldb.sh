#!/bin/bash

if [ -n "$1" ]
then
    echo Exporting Mysql database $1
else

	# Show usage and exit

    echo Usage: ./export-mysqldb [folder with mysqldb-credentials file]
    exit 0
fi

source $1/mysqldb-credentials

mysqldump -u$DBUSER -p$DBPASSWD -h$DBHOST $DBNAME > $1/mysqldb-data.sql

