#!/bin/bash

if [ -n "$1" ]
then
    echo Importing Mysql database $1
else

	# Show usage and exit

    echo Usage: ./import-mysqldb [folder with cleardb-credentials file]
    exit 0
fi

source $1/mysqldb-credentials

mysql -u$DBUSER -p$DBPASSWD -h$DBHOST $DBNAME < $1/mysqldb-data.sql

echo "NOTE: Don't forget to update Settings->Site Adress(URL) with the URL for the server!!"
